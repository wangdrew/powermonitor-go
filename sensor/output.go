package main

type Output interface {
	Start(metrics chan PowerMetrics, stop chan struct{})
}
