package main

// Clone replicates messages from the in channel into output channels which are returned in a slice
func Clone(in chan PowerMetrics, numCopies int) []chan PowerMetrics {
	bufLen := cap(in)
	out := make([]chan PowerMetrics, numCopies)
	for i, _ := range out {
		out[i] = make(chan PowerMetrics, bufLen)
	}
	go func() {
		for {
			msg := <-in
			for _, c := range out {
				select {
				case c <- msg:
				default:
					// Continue on to other channels if this channel is full
					continue
				}
			}
		}
	}()
	return out
}
