package main

type PowerMetric struct {
	SensorName string
	PowerW     float64   `json:"powerW"`
	EnergyWs   float64   `json:"energyWs"`
	VoltageV   float64   `json:"voltageV"`
}

type PowerMetrics []PowerMetric
