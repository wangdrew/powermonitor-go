package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestClone(t *testing.T) {
	n := 3
	numMsg := 10
	in := make(chan PowerMetrics, numMsg)
	cloned := Clone(in, n)
	assert.Len(t, cloned, n)
	for i := 0; i < numMsg; i++ {
		in <- PowerMetrics{{}}
	}
	time.Sleep(50 * time.Millisecond) // give goroutine time to run
	for _, ch := range cloned {
		assert.Len(t, ch, numMsg)
	}
}

func TestFullChannel(t *testing.T) {
	n := 2
	numMsg := 2
	in := make(chan PowerMetrics, numMsg)
	cloned := Clone(in, n)
	// fill up one of the output channels
	cloned[0] <- PowerMetrics{}
	cloned[0] <- PowerMetrics{}
	assert.Len(t, cloned[0], 2)

	// the other output channel should continue to fill up
	in <- PowerMetrics{}
	time.Sleep(50 * time.Millisecond)
	assert.Len(t, cloned[1], 1)
	in <- PowerMetrics{}
	time.Sleep(50 * time.Millisecond)
	assert.Len(t, cloned[1], 2)
}
