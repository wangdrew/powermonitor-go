package main

type Source interface {
	Init() error
	Read() (PowerMetrics, error)
}
