package main

type Publisher interface {
	Publish(<-chan PowerMetric)
}
