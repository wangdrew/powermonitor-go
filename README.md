## Powermonitor-go
Project details [TBD]

### Build
To build manually for arm6:
```shell script
docker-compose -f docker-compose-arm6.yml build

```
This compiles and builds the sensor docker images

### Running on a Raspberry Pi
If using Influx Cloud, ensure the influx cloud related envvars are set in `sensor.env`
Runs all containers. Requires docker and docker-compose
```shell script
docker-compose -f docker-compose-arm6.yml pull && docker-compose -f docker-compose-arm6.yml up --no-build
```
This runs the sensor service

Alternatively run:
```shell script
./run.sh
```
Append `-d` to run detached
