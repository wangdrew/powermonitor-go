import groovy.json.JsonSlurper

metadata {
    definition(name: "Power Monitor", namespace: "community", author: "Andrew Wang", importUrl: "todo", description: "Displays Power measurements from a Power Monitor device") {
        capability "Initialize"
    }
}

preferences {
    input name: "logEnable", type: "bool", title: "Enable debug logging", required: true, defaultValue: true
    input name: "deviceHostname", type: "text", title: "URL of the Power Monitor device", required: true, defaultValue: "192.168.69.93"
    input name: "topicSub", type: "text", title: "mqtt topic for power data", required: true, defaultValue: "power/+"
}

def logsOff(){
    log.warn "debug logging disabled..."
    device.updateSetting("logEnable",[value:"false",type:"bool"])
}

def updated(){
    log.info "updated!!"
    initialize()
    if (logEnable) runIn(1800,logsOff)
}

def installed() {
    log.info "installed!!"
    updated()
}

def healthcheck() {
    if (!interfaces.mqtt.isConnected()) {
        log.info "mqtt is down - attempting reconnection"
        initialize()
    }
    runIn(300, healthcheck)
}

def parse(String description) {
    JsonSlurper parser = new JsonSlurper()
    //log.debug(interfaces.mqtt.parseMessage(description).topic)
    String jsondata = interfaces.mqtt.parseMessage(description).payload
    Map parsed = parser.parseText(jsondata)
    String childName = interfaces.mqtt.parseMessage(description).topic.split('/').last()
    def child = getChildDevice(childDeviceId(childName))
    if (child == null) {
        if (logEnable) log.debug "child with name=${childName} does not exist."
        child = createChild(childName)
    }
    child.parse(parsed)
}

def initialize() {
    log.info "intialized!"
    try {
        String mqttbroker = "tcp://" + settings?.deviceHostname + ":1883"
        String clientname = "hubitat" + new Date().getTime()
        log.info "clientname: ${clientname}"
        log.debug "${mqttbroker}"
        interfaces.mqtt.disconnect() // cleanup old connection if any
        interfaces.mqtt.connect(mqttbroker, clientname , null, null)
        pauseExecution(1000) //give it a chance to start
        log.info "Connection established"

        if (logEnable) log.debug "Subscribed to: ${settings?.topicSub}"
        interfaces.mqtt.subscribe(settings?.topicSub)
    } catch(e) {
        if (logEnable) log.debug "Initialize error: ${e.message}"
    }
    runIn(300, healthcheck)
}

private def createChild(String childName) {
    def child = null
    log.info "Attempting to create child with name=" + childName;
    try {
        child = addChildDevice("Child Power Meter", childDeviceId(childName),
            [label: "${device.displayName} (${childName})",
             isComponent: false, name: "${childName}"])
        log.info "Created child device with network id: ${childDeviceId(childName)}"
    }
    catch(e) {
        log.error "Failed to create child device with error = ${e.message}";
    }
    return child
}

private String childDeviceId(String childName) {
    return "${device.deviceNetworkId}_${childName}"
}

def uninstalled() {
    if (logEnable) log.info "Disconnecting from mqtt"
    interfaces.mqtt.disconnect()
}
