## Hubitat Driver
These driver files allow integration between the powermonitor and a [Hubitat](https://hubitat.com/https://hubitat.com/) smart-home hub

Specifically, the drivers when loaded into Hubitat, will appear as a Virtual Device in a Hubitat ecosystem and provides power, voltage, and energy usage monitoring.

The hubitat driver subscribes to the MQTT message bus hosted by the Raspberry Pi and subscribes to the `Power` topic.

### Installation
#### Create Hubitat Driver
- Copy the entire contents of the ["power-monitor-hubitat-parent-driver.groovy"](power-monitor-hubitat-parent-driver.groovy) driver file from this repository.
- In your Hubitat Elevation Hub's admin web page, select the "Drivers Code" section and then click the "+ New Driver" button in the top right corner.  This will open a editor window for manipulating source code.
- Click in the editor window.  Then PASTE all of the code you copied in the first step.
- Click the SAVE button in the editor window.
- Repeat this process for ["power-monitor-hubitat-child-driver.groovy"](power-monitor-hubitat-child-driver.groovy)

#### Create the Powermonitor Hubitat Device
- In your Hubitat Elevation Hub's web page, select the "Devices" section, and then click the "+ Add Virtual Device" button in the top right corner.
- In the window that appears, please fill in the "Device Name", "Device Label", and "Device Network Id" fields.  Make sure the Device Network Id field is UNIQUE!  For example:
  - "Device Name" = Home Power Monitor
  - "Device Label" = {optional}
  - "Device Network Id" = (automatically filled in by Hubitat)
  - In the "Type" field, scroll to the bottom of the list and select "Power Monitor" (not the child)
- Click Save
- In the next window that appears, fill in the IP/hostname of the raspberry Pi or the thing running the MQTT broker.
- Click Save
- Look at Live Logs (or Past Logs if you didn't have a tab open when you clicked SAVE)
- You should see all of the Child Devices created for the Power Monitor
- You can now add these devices to the Hubitat Dashboard, use them in Rules, etc...  One nice use is for monitoring laundry cycles to create notifications
