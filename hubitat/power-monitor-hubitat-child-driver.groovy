metadata {
	definition (name: "Child Power Meter", namespace: "community", author: "Andrew Wang", importUrl: "todo") {
        capability "PowerMeter"
        capability "EnergyMeter"
        capability "VoltageMeasurement"
		capability "Sensor"
	}
   
    preferences {
        input name: "logEnable", type: "bool", title: "Enable debug logging", defaultValue: true
	}
}

def logsOff(){
    log.warn "debug logging disabled..."
    device.updateSetting("logEnable",[value:"false",type:"bool"])
}

def parse(Map data) {
    try {
        power = Math.round(Float.parseFloat("${data.powerW}"))
        voltage = String.format("%.1f", Float.parseFloat("${data.voltageV}"))
        energy = String.format("%.1f", Float.parseFloat("${data.energyWs}") / 3.6e6)

        sendEvent(name: "power", value: "${power} W")
        sendEvent(name: "voltage", value: "${voltage} V")
        sendEvent(name: "energy", value: "${energy} KwH")
    } catch(e) {
        if (logEnable) log.debug "Parse error: ${e.message} for ${data}"
    }
}

def installed() {
    updated()
}

def updated() {
    if (logEnable) runIn(1800,logsOff)
}